package ru.surfstudio.pokerexample.entities;

/**
 * класс игральной карты
 */
public class Card implements Comparable<Card>{
    private Suit suit;
    private CardValue cardValue;

    public Card(Suit suit, CardValue cardValue) {
        this.suit = suit;
        this.cardValue = cardValue;
    }

    public Suit getSuit() {
        return suit;
    }

    public CardValue getCardValue() {
        return cardValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Card)) return false;

        Card card = (Card) o;

        if (suit != card.suit) return false;
        return cardValue == card.cardValue;

    }

    @Override
    public int hashCode() {
        int result = suit != null ? suit.hashCode() : 0;
        result = 31 * result + (cardValue != null ? cardValue.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(Card another) {
        return this.getCardValue().getValue() - another.getCardValue().getValue();
    }
}
