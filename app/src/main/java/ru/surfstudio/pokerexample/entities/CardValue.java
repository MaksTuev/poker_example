package ru.surfstudio.pokerexample.entities;

/**
 * значение карты
 */
public enum CardValue
{
    TWO(0),
    THREE(1),
    FOUR(2),
    FIVE(3),
    SIX(4),
    SEVEN(5),
    EIGHT(6),
    NINE(7),
    TEN(8),
    JACK(9),
    QUEEN(10),
    KING(11),
    ACE(12);

    private final int value;

    CardValue(int value){
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static CardValue get(int valueInt) {
        for(CardValue cardValue: values()){
            if(cardValue.getValue() == valueInt){
                return cardValue;
            }
        }
        throw new IllegalArgumentException("value: " + valueInt);
    }
}
