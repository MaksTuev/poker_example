package ru.surfstudio.pokerexample.entities;

public enum GamePhase {
    NOT_STARTED, //игра не начиналась
    PREFLOP,
    FLOP,
    LONG,
    RIVER,
    END_LAP, // раздача закончена
    END_GAME //игра закончена
}
