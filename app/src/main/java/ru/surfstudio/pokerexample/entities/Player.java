package ru.surfstudio.pokerexample.entities;

import java.util.ArrayList;

public class Player {
    public static final int INITIAL_CASH = 50;
    private String id;
    private String name;
    private int cash = INITIAL_CASH;
    private ArrayList<Card> cards = new ArrayList<>();
    private ArrayList<PlayerAction> supportedActions = new ArrayList<>();
    private int bet;
    private boolean active;
    private boolean inGame;

    public Player(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public boolean isInGame() {
        return inGame;
    }

    public void setInGame(boolean inGame) {
        this.inGame = inGame;
    }

    public ArrayList<PlayerAction> getSupportedActions() {
        return supportedActions;
    }

    public void setSupportedActions(ArrayList<PlayerAction> supportedActions) {
        this.supportedActions = supportedActions;
    }

    public void setCash(int cash) {
        this.cash = cash;
    }

    public void setCards(ArrayList<Card> cards) {
        this.cards = cards;
    }

    public void setBet(int bet) {
        this.bet = bet;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getCash() {
        return cash;
    }

    public ArrayList<Card> getCards() {
        return cards;
    }

    public int getBet() {
        return bet;
    }

    public boolean isActive() {
        return active;
    }

    public void doBet(int bet) {
        this.bet += bet;
        this.cash -= bet;
        if (cash < 0) {
            throw new IllegalStateException();
        }
    }
}
