package ru.surfstudio.pokerexample.entities;

public enum PlayerAction {
    CHECK,
    CALL,
    RAISE,
    FOLD
}
