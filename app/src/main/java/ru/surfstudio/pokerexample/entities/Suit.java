package ru.surfstudio.pokerexample.entities;

/**
 * масть
 */
public enum Suit
{
    HEARTS,
    CLUBS,
    DIAMONDS,
    SPADES
}
