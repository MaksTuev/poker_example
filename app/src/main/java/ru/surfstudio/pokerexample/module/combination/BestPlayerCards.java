package ru.surfstudio.pokerexample.module.combination;

import java.util.ArrayList;

import ru.surfstudio.pokerexample.entities.Card;

/**
 * класс содержащий 5 карт с наилучшей комбинацией, выбранных из 2 карт игрока и 5 общих карт
 */
public class BestPlayerCards implements Comparable<BestPlayerCards> {
    private CardList nonCombinationCards = new CardList();
    private Combination combination;

    public CardList getNonCombinationCards() {
        return nonCombinationCards;
    }

    public Combination getCombination() {
        return combination;
    }

    public BestPlayerCards(ArrayList<Card> nonCombinationCards, Combination combination) {
        this.nonCombinationCards = new CardList(nonCombinationCards);
        this.combination = combination;
    }

    @Override
    public int compareTo(BestPlayerCards another) {
        int result = this.getCombination().compareTo(another.getCombination());
        if (result != 0) {
            return result;
        }
        CardList firstNonCombinationCards = this.getNonCombinationCards();
        CardList secondNonCombinationCards = this.getNonCombinationCards();
        firstNonCombinationCards.sortDesc();
        secondNonCombinationCards.sortDesc();
        for(int i = 0; i< firstNonCombinationCards.size(); i++){
            result = firstNonCombinationCards.get(i).compareTo(secondNonCombinationCards.get(i));
            if(result != 0){
                return result;
            }
        }
        return 0;


    }
}
