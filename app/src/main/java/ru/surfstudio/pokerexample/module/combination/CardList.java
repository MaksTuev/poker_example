package ru.surfstudio.pokerexample.module.combination;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ru.surfstudio.pokerexample.entities.Card;
import ru.surfstudio.pokerexample.entities.CardValue;
import ru.surfstudio.pokerexample.entities.Suit;

public class CardList extends ArrayList<Card> {

    public CardList(List<Card> cards) {
        super(cards);
    }

    public CardList() {
    }

    public boolean contains(Card card){
        for(Card c : this){
            if(c == card){
                return true;
            }
        }
        return false;
    }


    public CardList find(CardValue cardValue) {
        CardList result = new CardList();
        for(Card c : this){
            if(c.getCardValue() == cardValue){
                result.add(c);
            }
        }
        return result;
    }

    public CardList find(Suit suit) {
        CardList result = new CardList();
        for(Card c : this){
            if(c.getSuit() == suit){
                result.add(c);
            }
        }
        return result;
    }

    public void sortDesc(){
        Collections.sort(this, new Comparator<Card>() {
            @Override
            public int compare(Card lhs, Card rhs) {
                return lhs.compareTo(rhs);
            }
        });
    }

    public Card getHighCard() {
        Card result = this.get(0);
        for(Card c : this){
            result = result.compareTo(c) > 0 ? result : c;
        }
        return result;
    }

    public Card getHighCard(CardValue exclude) {
        Card result = this.get(0);
        for(Card c : this){
            result = result.compareTo(c) > 0 || c.getCardValue() == exclude ? result : c;
        }
        return result;
    }
}
