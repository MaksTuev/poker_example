package ru.surfstudio.pokerexample.module.combination;

import java.util.ArrayList;

import ru.surfstudio.pokerexample.entities.Card;

public class Combination implements Comparable<Combination>{
    private CombinationType combinationType;
    private CardList cards;

    public Combination(CombinationType combinationType, ArrayList<Card> cards) {
        this.combinationType = combinationType;
        this.cards = new CardList(cards);
    }

    public CombinationType getCombinationType() {
        return combinationType;
    }

    public CardList getCards() {
        return cards;
    }

    @Override
    public int compareTo(Combination another) {
        int result = combinationType.getValue() - another.getCombinationType().getValue();
        if(result != 0){
            return result;
        }
        //сравниваем комбинации с одинаковым типом
        switch (combinationType){
            case HIGH_CARD:
            case ONE_PAIR:
            case THREE_OF_A_KING:
            case STRAIGHT:
            case FLUSH:
            case FOUR_OF_A_KING:
            case STRAIGHT_FLASH:
                return compareHighCardInCombinations(this, another);
            case TWO_PAIR:
                return compareTwoPairCombinations(this, another);
            case FULL_HOUSE:
                return compareFullHouseCombinations(this, another);
            default:
                throw new IllegalStateException();
        }

    }


    private int compareTwoPairCombinations(Combination left, Combination right) {
        Card leftHighCard =left.getCards().getHighCard();
        Card rightHighCard =right.getCards().getHighCard();
        int result =  leftHighCard.compareTo(rightHighCard);
        if(result != 0){
            return result;
        }
        Card leftSecondCard =left.getCards().getHighCard(leftHighCard.getCardValue());
        Card rightSecondCard =right.getCards().getHighCard(rightHighCard.getCardValue());
        return leftSecondCard.compareTo(rightSecondCard);
    }

    private int compareFullHouseCombinations(Combination left, Combination right) {
        Card leftHighCard =left.getCards().getHighCard();
        Card rightHighCard =right.getCards().getHighCard();
        int result =  leftHighCard.compareTo(rightHighCard);
        if(result != 0){
            return result;
        }
        Card leftSecondCard =left.getCards().getHighCard(leftHighCard.getCardValue());
        Card rightSecondCard =right.getCards().getHighCard(rightHighCard.getCardValue());
        return leftSecondCard.compareTo(rightSecondCard);
    }

    private int compareHighCardInCombinations(Combination left, Combination right) {
        Card leftHighCard =left.getCards().get(0);
        Card rightHighCard =right.getCards().get(0);
        return  leftHighCard.compareTo(rightHighCard);
    }
}
