package ru.surfstudio.pokerexample.module.combination;

public enum CombinationType {
    HIGH_CARD(0),
    ONE_PAIR(1),
    TWO_PAIR(2),
    THREE_OF_A_KING(3),
    STRAIGHT(4),
    FLUSH(5),
    FULL_HOUSE(6),
    FOUR_OF_A_KING(7),
    STRAIGHT_FLASH(8);

    private final int value;

    CombinationType(int value){
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
