package ru.surfstudio.pokerexample.module.combination;

import java.util.List;
import java.util.Map;

import ru.surfstudio.pokerexample.entities.Card;

/**
 * позволяет определить победителя раздачи
 */
public interface WinnerFinder {
    /**
     * выбирает игрока с наилучшей комбинацией
     * @param userCards - карты игроков
     * @param tableCards - общие карты
     * @return id победителя
     */
    String find(Map<String, List<Card>> userCards, List<Card> tableCards);
}
