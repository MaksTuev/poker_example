package ru.surfstudio.pokerexample.module.combination;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import ru.surfstudio.pokerexample.entities.Card;
import ru.surfstudio.pokerexample.module.combination.finder.CombinationFinder;
import ru.surfstudio.pokerexample.module.combination.finder.FlushCombinationFinder;
import ru.surfstudio.pokerexample.module.combination.finder.FourOfAKingCombinationFinder;
import ru.surfstudio.pokerexample.module.combination.finder.FullHouseCombinationFinder;
import ru.surfstudio.pokerexample.module.combination.finder.HightCardCombinationFinder;
import ru.surfstudio.pokerexample.module.combination.finder.OnePairCombinationFinder;
import ru.surfstudio.pokerexample.module.combination.finder.StraightCombinationFinder;
import ru.surfstudio.pokerexample.module.combination.finder.StraightFlushCombinationFinder;
import ru.surfstudio.pokerexample.module.combination.finder.ThreeOfAKingCombinationFinder;
import ru.surfstudio.pokerexample.module.combination.finder.TwoPairCombinationFinder;

public class WinnerFinderImpl implements WinnerFinder {

    private List<CombinationFinder> combinationFinders = Arrays.asList(
            new StraightFlushCombinationFinder(),
            new FourOfAKingCombinationFinder(),
            new FullHouseCombinationFinder(),
            new FlushCombinationFinder(),
            new StraightCombinationFinder(),
            new ThreeOfAKingCombinationFinder(),
            new TwoPairCombinationFinder(),
            new OnePairCombinationFinder(),
            new HightCardCombinationFinder());

    @Override
    public String find(Map<String, List<Card>> playersHandCards, List<Card> tableCards) {
        String winner = null;
        BestPlayerCards winnerBestPlayerCards = null;
        for (Map.Entry<String, List<Card>> playerHandCards : playersHandCards.entrySet()) {
            ArrayList<Card> allPlayerCards = new ArrayList<>();
            allPlayerCards.addAll(playerHandCards.getValue());
            allPlayerCards.addAll(tableCards);
            BestPlayerCards bestPlayerCards = getBestPlayerCards(allPlayerCards);
            if (winnerBestPlayerCards == null) {
                winnerBestPlayerCards = bestPlayerCards;
                winner = playerHandCards.getKey();
            } else {
                boolean newWinner = winnerBestPlayerCards.compareTo(bestPlayerCards) < 0;
                if (newWinner) {
                    winnerBestPlayerCards = bestPlayerCards;
                    winner = playerHandCards.getKey();
                }
            }
        }
        return winner;
    }

    private BestPlayerCards getBestPlayerCards(List<Card> allPlayerCards) {
        CardList playerCardList = new CardList(allPlayerCards);
        Combination combination = findCombination(allPlayerCards);
        playerCardList.sortDesc();
        ArrayList<Card> nonCombinationCards = new ArrayList<>();
        int numNonCombinationCards = 5 - combination.getCards().size();
        int i = 0;
        while (nonCombinationCards.size() < numNonCombinationCards) {
            Card card = playerCardList.get(i);
            i++;
            if (combination.getCards().contains(card)) {
                continue;
            } else {
                nonCombinationCards.add(card);
            }
        }
        BestPlayerCards result = new BestPlayerCards(nonCombinationCards, combination);
        return result;
    }

    private Combination findCombination(List<Card> allPlayerCards) {
        for (CombinationFinder combinationFinder : combinationFinders) {
            Combination result = combinationFinder.find(allPlayerCards);
            if (result != null) {
                return result;
            }
        }
        throw new IllegalStateException();
    }
}
