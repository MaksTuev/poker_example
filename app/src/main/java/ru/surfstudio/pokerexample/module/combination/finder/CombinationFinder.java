package ru.surfstudio.pokerexample.module.combination.finder;

import java.util.List;

import ru.surfstudio.pokerexample.entities.Card;
import ru.surfstudio.pokerexample.module.combination.Combination;

public interface CombinationFinder {
    Combination find(List<Card> cards);

}
