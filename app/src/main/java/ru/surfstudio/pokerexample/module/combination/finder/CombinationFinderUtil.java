package ru.surfstudio.pokerexample.module.combination.finder;

import java.util.ArrayList;
import java.util.List;

import ru.surfstudio.pokerexample.entities.CardValue;

public class CombinationFinderUtil {
    private static List<CardValue> cardValuesDesc;

    static {
        cardValuesDesc = new ArrayList<>();
        for(int i = CardValue.ACE.getValue(); i>= CardValue.TWO.getValue(); i--){
            cardValuesDesc.add(CardValue.get(i));
        }
    }

    public static List<CardValue> cardValuesDesc(){
        return new ArrayList<>(cardValuesDesc);
    }
}
