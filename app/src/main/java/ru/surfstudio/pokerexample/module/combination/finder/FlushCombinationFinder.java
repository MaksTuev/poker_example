package ru.surfstudio.pokerexample.module.combination.finder;

import java.util.ArrayList;
import java.util.List;

import ru.surfstudio.pokerexample.entities.Card;
import ru.surfstudio.pokerexample.entities.Suit;
import ru.surfstudio.pokerexample.module.combination.CardList;
import ru.surfstudio.pokerexample.module.combination.Combination;
import ru.surfstudio.pokerexample.module.combination.CombinationType;

public class FlushCombinationFinder implements CombinationFinder {
    @Override
    public Combination find(List<Card> cards) {
        CardList cardList = new CardList(cards);
        for (Suit suit: Suit.values()) {
            CardList foundCards = cardList.find(suit);
            if (foundCards.size() >= 5) {
                foundCards.sortDesc();
                ArrayList<Card> combinationCards = new ArrayList<>();
                combinationCards.addAll(foundCards.subList(foundCards.size() - 5, foundCards.size())); //берем старший флеш
                return new Combination(CombinationType.FLUSH, combinationCards);
            }
        }
        return null;
    }
}
