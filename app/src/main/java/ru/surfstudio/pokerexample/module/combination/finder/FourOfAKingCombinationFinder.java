package ru.surfstudio.pokerexample.module.combination.finder;

import java.util.List;

import ru.surfstudio.pokerexample.entities.Card;
import ru.surfstudio.pokerexample.entities.CardValue;
import ru.surfstudio.pokerexample.module.combination.CardList;
import ru.surfstudio.pokerexample.module.combination.Combination;
import ru.surfstudio.pokerexample.module.combination.CombinationType;

public class FourOfAKingCombinationFinder implements CombinationFinder{
    @Override
    public Combination find(List<Card> cards) {
        CardList cardList = new CardList(cards);
        Combination result = null;
        for(CardValue cardValue: CardValue.values()){
            CardList foundCards = cardList.find(cardValue);
            if(foundCards.size() == 4){
                result = new Combination(CombinationType.FOUR_OF_A_KING, foundCards);
                break;
            }
        }
        return result;
    }
}
