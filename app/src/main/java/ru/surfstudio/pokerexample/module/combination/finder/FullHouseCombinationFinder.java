package ru.surfstudio.pokerexample.module.combination.finder;

import java.util.ArrayList;
import java.util.List;

import ru.surfstudio.pokerexample.entities.Card;
import ru.surfstudio.pokerexample.entities.CardValue;
import ru.surfstudio.pokerexample.module.combination.CardList;
import ru.surfstudio.pokerexample.module.combination.Combination;
import ru.surfstudio.pokerexample.module.combination.CombinationType;

public class FullHouseCombinationFinder implements CombinationFinder {
    @Override
    public Combination find(List<Card> cards) {
        CardList cardList = new CardList(cards);
        for (CardValue cardValue3 : CombinationFinderUtil.cardValuesDesc()) {
            CardList found3Cards = cardList.find(cardValue3);
            if (found3Cards.size() >= 3) {
                for (CardValue cardValue2 : CombinationFinderUtil.cardValuesDesc()) {
                    if (cardValue2 == cardValue3) {
                        continue;
                    }
                    CardList found2Cards = cardList.find(cardValue2);
                    if(found2Cards.size() >= 2){
                        ArrayList<Card> combinationCards = new ArrayList<>();
                        combinationCards.addAll(found3Cards.subList(0, 3));
                        combinationCards.addAll(found2Cards.subList(0, 2));
                        Combination result = new Combination(CombinationType.FULL_HOUSE, combinationCards);
                        return result;
                    }
                }
            }
        }
        return null;
    }
}
