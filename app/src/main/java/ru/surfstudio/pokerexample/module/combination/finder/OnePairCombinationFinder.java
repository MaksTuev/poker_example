package ru.surfstudio.pokerexample.module.combination.finder;

import java.util.ArrayList;
import java.util.List;

import ru.surfstudio.pokerexample.entities.Card;
import ru.surfstudio.pokerexample.entities.CardValue;
import ru.surfstudio.pokerexample.module.combination.CardList;
import ru.surfstudio.pokerexample.module.combination.Combination;
import ru.surfstudio.pokerexample.module.combination.CombinationType;

public class OnePairCombinationFinder implements CombinationFinder {
    @Override
    public Combination find(List<Card> cards) {
        CardList cardList = new CardList(cards);
        for (CardValue cardValue : CombinationFinderUtil.cardValuesDesc()) {
            CardList foundCards = cardList.find(cardValue);
            if (foundCards.size() >= 2) {
                ArrayList<Card> combinationCards = new ArrayList<>();
                combinationCards.addAll(foundCards.subList(0, 2));
                Combination result = new Combination(CombinationType.ONE_PAIR, combinationCards);
                return result;
            }
        }
        return null;
    }
}
