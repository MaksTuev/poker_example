package ru.surfstudio.pokerexample.module.combination.finder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ru.surfstudio.pokerexample.entities.Card;
import ru.surfstudio.pokerexample.entities.CardValue;
import ru.surfstudio.pokerexample.module.combination.CardList;
import ru.surfstudio.pokerexample.module.combination.Combination;
import ru.surfstudio.pokerexample.module.combination.CombinationType;

public class StraightCombinationFinder implements CombinationFinder {

    @Override
    public Combination find(List<Card> cards) {
        CardList cardList = new CardList(cards);
        List<Combination> combinations = new ArrayList<>();
        for (int i = 0; i < cardList.size(); i++) {
            ArrayList<Card> combinationCards = new ArrayList<>();
            Card currentCard = cardList.get(i);
            combinationCards.add(currentCard);
            for (int j = 1; j < 5; j++) {
                if(currentCard.getCardValue() == CardValue.ACE && j != 1){
                    break;
                }
                CardValue nextCardValue = getNextCardValue(currentCard, j == 1);
                CardList findCards = cardList.find(nextCardValue);
                if (findCards.size() >= 1) {
                    Card nextCard = findCards.get(0);
                    combinationCards.add(nextCard);
                    currentCard = nextCard;
                } else {
                    break;
                }
            }
            if(combinationCards.size() == 5){
                Combination combination = new Combination(CombinationType.STRAIGHT, combinationCards);
                combinations.add(combination);
            }

        }
        if(combinations.size() == 0){
            return null;
        } else {
            Collections.sort(combinations); //выбираем старшую комбинацию
            return combinations.get(0);
        }
    }

    private CardValue getNextCardValue(Card currentCard, boolean firstCardInCombination) {
        int nextCardValueInt = firstCardInCombination && currentCard.getCardValue() == CardValue.ACE
                ? CardValue.TWO.getValue()
                : currentCard.getCardValue().getValue() + 1;
        CardValue nextCardValue = CardValue.get(nextCardValueInt);
        return nextCardValue;
    }
}
