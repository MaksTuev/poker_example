package ru.surfstudio.pokerexample.module.combination.finder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ru.surfstudio.pokerexample.entities.Card;
import ru.surfstudio.pokerexample.entities.CardValue;
import ru.surfstudio.pokerexample.module.combination.CardList;
import ru.surfstudio.pokerexample.module.combination.Combination;
import ru.surfstudio.pokerexample.module.combination.CombinationType;

public class StraightFlushCombinationFinder implements CombinationFinder {

    @Override
    public Combination find(List<Card> cards) {
        CardList cardList = new CardList(cards);
        List<Combination> combinations = new ArrayList<>();
        for (int i = 0; i < cardList.size(); i++) {
            ArrayList<Card> combinationCards = new ArrayList<>();
            Card currentCard = cardList.get(i);
            combinationCards.add(currentCard);
            for (int j = 1; j < 5; j++) {
                if(currentCard.getCardValue() == CardValue.ACE && j != 1){
                    break;
                }
                Card nextCard = getNextCard(currentCard, j == 1);
                if (cardList.contains(nextCard)) {
                    combinationCards.add(nextCard);
                    currentCard = nextCard;
                } else {
                    break;
                }
            }
            if(combinationCards.size() == 5){
                Combination combination = new Combination(CombinationType.STRAIGHT_FLASH, combinationCards);
                combinations.add(combination);
            }

        }
        if(combinations.size() == 0){
            return null;
        } else {
            Collections.sort(combinations); //выбираем старшую комбинацию
            return combinations.get(0);
        }


    }

    private Card getNextCard(Card currentCard, boolean firstCardInCombination) {
        int nextCardValueInt = firstCardInCombination && currentCard.getCardValue() == CardValue.ACE
                ? CardValue.TWO.getValue()
                : currentCard.getCardValue().getValue() + 1;
        CardValue nextCardValue = CardValue.get(nextCardValueInt);
        Card nextCard = new Card(currentCard.getSuit(), nextCardValue);
        return nextCard;
    }
}
