package ru.surfstudio.pokerexample.module.combination.finder;

import java.util.ArrayList;
import java.util.List;

import ru.surfstudio.pokerexample.entities.Card;
import ru.surfstudio.pokerexample.entities.CardValue;
import ru.surfstudio.pokerexample.module.combination.CardList;
import ru.surfstudio.pokerexample.module.combination.Combination;
import ru.surfstudio.pokerexample.module.combination.CombinationType;

public class TwoPairCombinationFinder implements CombinationFinder {
    @Override
    public Combination find(List<Card> cards) {
        CardList cardList = new CardList(cards);
        for (CardValue cardValueFirst : CombinationFinderUtil.cardValuesDesc()) {
            CardList foundFirst2Cards = cardList.find(cardValueFirst);
            if (foundFirst2Cards.size() >= 2) {
                for (CardValue cardValueSecond : CombinationFinderUtil.cardValuesDesc()) {
                    if (cardValueSecond == cardValueFirst) {
                        continue;
                    }
                    CardList foundSecond2Cards = cardList.find(cardValueSecond);
                    if(foundSecond2Cards.size() >= 2){
                        ArrayList<Card> combinationCards = new ArrayList<>();
                        combinationCards.addAll(foundFirst2Cards.subList(0, 2));
                        combinationCards.addAll(foundSecond2Cards.subList(0, 2));
                        Combination result = new Combination(CombinationType.TWO_PAIR, combinationCards);
                        return result;
                    }
                }
            }
        }
        return null;
    }
}
