package ru.surfstudio.pokerexample.module.game;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ru.surfstudio.pokerexample.entities.Card;
import ru.surfstudio.pokerexample.entities.CardValue;
import ru.surfstudio.pokerexample.entities.Suit;

public class DeckCards {
    static int NUM_OF_SHUFFLE = 150;
    List<Card> deck;
    int position;

    public DeckCards() {
        position = 0;
        deck = new ArrayList<Card>(52);
        for (Suit s : Suit.values()) {
            for (CardValue v : CardValue.values()) {
                deck.add(new Card(s, v));
            }
        }
        shuffle();
    }

    private void shuffle() {
        position = 0;
        Random rnd = new Random();
        for (int i = 0; i < NUM_OF_SHUFFLE; i++) {
            int one = rnd.nextInt(deck.size());
            int two = rnd.nextInt(deck.size());
            Card tmp = deck.get(one);
            deck.set(one, deck.get(two));
            deck.set(two, tmp);
        }
    }

    public Card getCard() {
        try {
            return deck.get(position++);
        } catch (IndexOutOfBoundsException e) {
            throw new RuntimeException("Колода пуста");
        }
    }

    public ArrayList<Card> getCards(int num) {
        ArrayList<Card> result = new ArrayList<>(num);
        for(int i = 0; i< num; i++){
            result.add(getCard());
        }
        return result;
    }
}
