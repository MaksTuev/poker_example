package ru.surfstudio.pokerexample.module.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.surfstudio.pokerexample.entities.Card;
import ru.surfstudio.pokerexample.entities.GamePhase;
import ru.surfstudio.pokerexample.entities.Player;
import ru.surfstudio.pokerexample.entities.PlayerAction;
import ru.surfstudio.pokerexample.module.combination.WinnerFinder;
import ru.surfstudio.pokerexample.module.combination.WinnerFinderImpl;
import ru.surfstudio.pokerexample.util.SimpleStickyOnSubscribe;
import rx.Observable;

/**
 * Round - раунд ставок
 * Lap - раздача
 * Game - игра
 */
public class GameManager {
    private final int BLIND_VALUE = 1; //$
    private final int BET_VALUE = 1;//$

    private static GameManager instance;

    private PlayersCyclicList players = new PlayersCyclicList(new ArrayList<>());
    private String dealer; //id диллера
    private String activePlayer; //id активного плеера
    private String lastPlayerInRound; //id последнего игрока в раунде
    private String playerWithGreatestBet; //id игрока, имеющего наибольшую ставку
    private ArrayList<Card> tableCards = new ArrayList<>(5);
    private GamePhase gamePhase = GamePhase.NOT_STARTED;
    private int bank;
    private DeckCards deckCards;
    private SimpleStickyOnSubscribe<GameStateChangedEvent> gameStateChangedEventEmitter = new SimpleStickyOnSubscribe<>();
    private WinnerFinder winnerFinder = new WinnerFinderImpl();

    public static GameManager getInstance() {
        if (instance == null) {
            instance = new GameManager();
        }
        return instance;
    }

    public GameManager() {
    }

    public Observable<GameStateChangedEvent> observeChangingGameState() {
        return Observable.create(gameStateChangedEventEmitter);
    }

    public void start() {
        for (Player player : players) {
            player.setCash(Player.INITIAL_CASH);
        }
        nextLap();
    }

    public void setPlayers(ArrayList<Player> players) {
        if (gamePhase != GamePhase.NOT_STARTED) {
            throw new UnsupportedActionException();
        }
        this.players = new PlayersCyclicList(players);
        notifyListeners();
    }

    public ArrayList<Player> getPlayers() {
        return players; //todo immutable
    }

    public void onCheck(String playerId) {
        Player player = players.get(playerId);
        checkSupportAction(player, PlayerAction.CHECK);
        onEndPlayerAction(false);
    }

    public void onCall(String playerId) {
        Player player = players.get(playerId);
        checkSupportAction(player, PlayerAction.CALL);
        int currentBet = getCurrentBet();
        int playerBet = player.getBet();
        int realBet = currentBet - playerBet;
        if (realBet < 0) {
            throw new IllegalStateException();
        }
        player.doBet(realBet);
        onEndPlayerAction(false);
    }

    public void onFold(String playerId) {
        Player player = players.get(playerId);
        checkSupportAction(player, PlayerAction.FOLD);
        player.setInGame(false);
        onEndPlayerAction(false);
    }

    public void onRaise(String playerId) {
        Player player = players.get(playerId);
        checkSupportAction(player, PlayerAction.RAISE);
        lastPlayerInRound = players.getPrevious(playerId).getId();
        playerWithGreatestBet = playerId;
        int currentBet = getCurrentBet();
        int playerBet = player.getBet();
        int realBet = currentBet - playerBet + BET_VALUE;
        player.doBet(realBet);
        onEndPlayerAction(true);
    }

    private void onEndPlayerAction(boolean lastActionIsRaise) {
        if (isEndOfRound(lastActionIsRaise)) {
            onEndOfRound();
        } else {
            activateNextPlayer();
            notifyListeners();
        }
    }

    private void onEndOfRound() {
        collectBetInBank();
        GamePhase nextGamePhase = getNextGamePhase(gamePhase);
        if (isEndOfLap(nextGamePhase)) {
            onEndOfLap();
        } else {
            nextRound(nextGamePhase);
        }
    }

    private void onEndOfLap() {
        resetRoundPlayersState();
        Player player = findWinner();
        player.setCash(player.getCash() + bank);
        bank = 0;
        gamePhase = endOfGame()
                ? GamePhase.END_GAME
                : GamePhase.END_LAP;
        notifyListeners(player.getId());
    }

    public void nextLap() {
        if (players.size() == 0) {
            throw new IllegalStateException();
        }
        resetLapPlayersState();
        dealer = dealer == null
                ? players.get(0).getId()
                : players.getNext(dealer).getId();
        players.get(dealer).doBet(BLIND_VALUE);
        playerWithGreatestBet = dealer;
        tableCards.clear();
        deckCards = new DeckCards();
        bank = 0;
        nextRound(GamePhase.PREFLOP);
    }


    private void nextRound(GamePhase gamePhase) {
        this.gamePhase = gamePhase;
        resetRoundPlayersState();
        lastPlayerInRound = dealer;
        passCards(gamePhase);
        activateNextPlayer();
        notifyListeners();
    }

    // -------- end of core logic ------

    private void resetRoundPlayersState() {
        activePlayer = null;
        updateRoundPlayersState();
    }

    private void resetLapPlayersState() {
        for (Player p : players) {
            p.setInGame(p.getCash() >= BLIND_VALUE);
        }
    }

    private void passCards(GamePhase gamePhase) {
        switch (gamePhase) {
            case PREFLOP:
                for (Player p : players.getInGame()) {
                    p.setCards(deckCards.getCards(2));
                }
                break;
            case FLOP:
                tableCards = deckCards.getCards(3);
                break;
            case LONG:
                tableCards.add(deckCards.getCard());
                break;
            case RIVER:
                tableCards.add(deckCards.getCard());
                break;
            default:
                throw new IllegalStateException();
        }
    }

    private void activateNextPlayer() {
        if (activePlayer == null) {
            activePlayer = players.getNext(dealer).getId();
        } else {
            activePlayer = players.getNext(activePlayer).getId();
        }
        updateRoundPlayersState();
    }

    private void updateRoundPlayersState() {
        for (Player p : players) {
            p.setActive(p.getId().equals(activePlayer));
            p.setSupportedActions(getSupportedActions(p));
        }

    }

    private void notifyListeners() {
        notifyListeners(null);
    }

    private void notifyListeners(String winnerPlayerId) {
        GameStateChangedEvent event = new GameStateChangedEvent(
                new ArrayList<>(players),
                bank,
                new ArrayList<>(tableCards),
                winnerPlayerId,
                gamePhase);
        gameStateChangedEventEmitter.emit(event);
    }

    private void collectBetInBank() {
        for (Player p : players.getInGame()) {
            bank += p.getBet();
            p.setBet(0);
        }
    }

    private boolean isEndOfRound(boolean lastActionIsRaise) {
        if(lastActionIsRaise){
            return players.getInGame().size() == 1;
        } else {
            return activePlayer.equals(lastPlayerInRound)
                    || players.getInGame().size() == 1;
        }
    }

    private GamePhase getNextGamePhase(GamePhase currentGamePhase) {
        if (players.getInGame().size() == 1) {
            return GamePhase.END_LAP;
        }
        switch (currentGamePhase) {
            case PREFLOP:
                return GamePhase.FLOP;
            case FLOP:
                return GamePhase.LONG;
            case LONG:
                return GamePhase.RIVER;
            case RIVER:
                return GamePhase.END_LAP;
            case END_LAP:
            default:
                throw new IllegalArgumentException();
        }
    }

    private void checkSupportAction(Player player, PlayerAction action) {
        if (!player.getSupportedActions().contains(action)) {
            throw new UnsupportedActionException();
        }
    }

    private boolean endOfGame() {
        for (Player p : players) {
            if (p.getCash() < BLIND_VALUE) {
                return true;
            }
        }
        return false;
    }

    private ArrayList<PlayerAction> getSupportedActions(Player p) { //todo disable raise for small bank
        if (!p.isActive()) {
            return new ArrayList<>();
        }
        ArrayList<PlayerAction> result = new ArrayList<>(Arrays.asList(PlayerAction.CHECK, PlayerAction.RAISE, PlayerAction.FOLD));
        Player playerWithGreatestBet = players.get(this.playerWithGreatestBet);
        if (p.getBet() < playerWithGreatestBet.getBet()) {
            result = new ArrayList<>(Arrays.asList(PlayerAction.CALL, PlayerAction.RAISE, PlayerAction.FOLD));
        }
        return result;
    }

    private int getCurrentBet() {
        int result = 0;
        for (Player p : players.getInGame()) {
            result = Math.max(result, p.getBet());
        }
        return result;
    }

    private Player findWinner() {
        if(players.getInGame().size() == 1){
            return players.getInGame().get(0);
        } else {
            Map<String, List<Card>> playerCards = new HashMap<>();
            for(Player player: players.getInGame()){
                playerCards.put(player.getId(), player.getCards());
            }
            String winnerId = winnerFinder.find(playerCards, tableCards);
            return players.get(winnerId);
        }
    }

    private boolean isEndOfLap(GamePhase nextGamePhase) {
        return nextGamePhase == GamePhase.END_LAP;
    }

}
