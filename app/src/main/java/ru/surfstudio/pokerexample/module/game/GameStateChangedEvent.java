package ru.surfstudio.pokerexample.module.game;

import java.util.ArrayList;

import ru.surfstudio.pokerexample.entities.Card;
import ru.surfstudio.pokerexample.entities.GamePhase;
import ru.surfstudio.pokerexample.entities.Player;

public class GameStateChangedEvent {
    private ArrayList<Player> players = new ArrayList<>(); //todo immutable
    private int bank;
    private ArrayList<Card> tableCards = new ArrayList<>();
    private String winnerId;
    private GamePhase gamePhase;

    public GameStateChangedEvent(ArrayList<Player> players, int bank, ArrayList<Card> tableCards, String winnerId, GamePhase gamePhase) {
        this.players = players;
        this.bank = bank;
        this.tableCards = tableCards;
        this.winnerId = winnerId;
        this.gamePhase = gamePhase;
    }

    public GamePhase getGamePhase() {
        return gamePhase;
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public int getBank() {
        return bank;
    }

    public ArrayList<Card> getTableCards() {
        return tableCards;
    }

    public String getWinnerId() {
        return winnerId;
    }
}
