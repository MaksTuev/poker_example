package ru.surfstudio.pokerexample.module.game;

import java.util.ArrayList;
import java.util.List;

import ru.surfstudio.pokerexample.entities.Player;
import rx.Observable;

public class PlayersCyclicList extends ArrayList<Player> {
    public PlayersCyclicList(ArrayList<Player> players) {
        super(players);
    }

    public Player getNext(String previousPlayerId) {
        String startPlayerId = previousPlayerId;
        Player nextPlayer = null;
        do {
            Player previousPlayer = get(previousPlayerId);
            if (previousPlayer == null) {
                throw new IllegalArgumentException();
            }
            int prevPlayerPosition = this.indexOf(previousPlayer);
            int nextPlayerPosition = prevPlayerPosition == this.size() - 1
                    ? 0
                    : prevPlayerPosition + 1;
            nextPlayer = this.get(nextPlayerPosition);
            if(nextPlayer.getId().equals(startPlayerId)){
                throw new IllegalStateException(); //зациклились
            }
            previousPlayerId = nextPlayer.getId();
        } while (!nextPlayer.isInGame());
        return nextPlayer;
    }

    public Player getPrevious(String currentPlayerId) {
        String startPlayerId = currentPlayerId;
        Player prevPlayer = null;
        do {
            Player currentPlayer = get(currentPlayerId);
            if (currentPlayer == null) {
                throw new IllegalArgumentException();
            }
            int currentPlayerPosition = this.indexOf(currentPlayer);
            int prevPlayerPosition = currentPlayerPosition == 0
                    ? this.size() - 1
                    : currentPlayerPosition - 1;
            prevPlayer = this.get(prevPlayerPosition);
            if(prevPlayer.getId().equals(startPlayerId)){
                throw new IllegalStateException(); //зациклились
            }
            currentPlayerId = prevPlayer.getId();
        } while (!prevPlayer.isInGame());
        return prevPlayer;
    }

    public Player get(String id) {
        for (Player p : this) {
            if (p.getId().equals(id)) {
                return p;
            }
        }
        return null;
    }

    public List<Player> getInGame(){
        return Observable.from(this)
                .filter(player->player.isInGame())
                .toList()
                .toBlocking()
                .first();
    }
}
