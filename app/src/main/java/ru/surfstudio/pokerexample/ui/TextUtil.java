package ru.surfstudio.pokerexample.ui;

import ru.surfstudio.pokerexample.entities.Card;

public class TextUtil {
    
    public static String getCardText(Card card) {
        String result;
        result = getCardValueText(card);

        switch (card.getSuit()) {
            case HEARTS:
                result += "♥";
                break;
            case CLUBS:
                result += "♣";
                break;
            case DIAMONDS:
                result += "♦";
                break;
            case SPADES:
                result += "♠";
                break;
            default:
                throw new IllegalArgumentException();
        }
        return result;
    }

    private static String getCardValueText(Card card) {
        switch (card.getCardValue()) {
            case TWO:
                return "2";
            case THREE:
                return "3";
            case FOUR:
                return "4";
            case FIVE:
                return "5";
            case SIX:
                return "6";
            case SEVEN:
                return "7";
            case EIGHT:
                return "8";
            case NINE:
                return "9";
            case TEN:
                return "10";
            case JACK:
                return "J";
            case QUEEN:
                return "Q";
            case KING:
                return "K";
            case ACE:
                return "A";
            default:
                throw new IllegalArgumentException();
        }
    }
}
