package ru.surfstudio.pokerexample.ui.main;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

import ru.surfstudio.pokerexample.R;
import ru.surfstudio.pokerexample.entities.Card;
import ru.surfstudio.pokerexample.entities.GamePhase;
import ru.surfstudio.pokerexample.entities.Player;
import ru.surfstudio.pokerexample.entities.PlayerAction;
import ru.surfstudio.pokerexample.module.game.GameManager;
import ru.surfstudio.pokerexample.module.game.GameStateChangedEvent;
import ru.surfstudio.pokerexample.ui.main.view.CardListView;
import ru.surfstudio.pokerexample.ui.main.view.player.PlayerView;
import rx.Subscription;

public class MainActivity extends AppCompatActivity {
    private static final String AI_PLAYER_ID = "AI_PLAYER_ID";
    private static final String REAL_PLAYER_ID = "REAL_PLAYER_ID";

    private GameManager gameManager;

    private PlayerView realPlayerView;
    private PlayerView aiPlayerView;
    private TextView bankText;
    private TextView whoWinText;
    private CardListView tableCardsView;
    private View newLapBtn;
    private View newGameBtn;
    private View raiseBtn;
    private View callBtn;
    private View checkBtn;
    private View foldBtn;

    private Subscription changingGameStateSubscription;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gameManager = GameManager.getInstance();
        findViews();
        initViews();
        initListeners();
        initGame();
    }

    private void initGame() {
        if (gameManager.getPlayers().size() == 0) {
            addPlayers();
        }
        changingGameStateSubscription = gameManager.observeChangingGameState()
                .subscribe(event -> onGameStateChanged(event));
    }

    private void addPlayers() {
        ArrayList<Player> players = new ArrayList<>();
        players.add(new Player(REAL_PLAYER_ID, "Игрок 1"));
        players.add(new Player(AI_PLAYER_ID, "Компьютер"));
        gameManager.setPlayers(players);
    }

    private void onGameStateChanged(GameStateChangedEvent event) {
        updatePlayerViews(event);
        showTableCards(event);
        showBank(event);
        updatePlayerActionButtons(event);
        showState(event);
        tryMakeAiPlayerAction(event);
    }

    private void showState(GameStateChangedEvent event) {
        GamePhase gamePhase = event.getGamePhase();
        if (gamePhase == GamePhase.END_LAP) {
            showEndLapState(event);
        } else if (gamePhase == GamePhase.END_GAME) {
            showEndGameState(event);
        } else if (gamePhase == GamePhase.NOT_STARTED) {
            showInitialState(event);
        } else {
            showRunningState();
        }
    }

    private void showTableCards(GameStateChangedEvent event) {
        ArrayList<Card> tableCards = new ArrayList<>(event.getTableCards());
        int lakingCards = 5 - tableCards.size();
        for (int i = 0; i < lakingCards; i++) {
            tableCards.add(null);
        }
        tableCardsView.show(tableCards);
    }

    private void showBank(GameStateChangedEvent event) {
        bankText.setText(event.getBank() != 0
                ? getResources().getString(R.string.bank_text, event.getBank())
                : "");
    }

    private void showInitialState(GameStateChangedEvent event) {
        newGameBtn.setVisibility(View.VISIBLE);
        newLapBtn.setVisibility(View.GONE);
        tableCardsView.setVisibility(View.GONE);
        whoWinText.setVisibility(View.GONE);
    }

    private void showEndLapState(GameStateChangedEvent event) {
        Player winner = getPlayer(event.getPlayers(), event.getWinnerId());
        whoWinText.setText(getString(R.string.winner_text, winner.getName()));
        whoWinText.setVisibility(View.VISIBLE);
        newLapBtn.setVisibility(View.VISIBLE);
        newGameBtn.setVisibility(View.GONE);
    }

    private void showRunningState() {
        newGameBtn.setVisibility(View.GONE);
        newLapBtn.setVisibility(View.GONE);
        whoWinText.setVisibility(View.GONE);
        tableCardsView.setVisibility(View.VISIBLE);
    }

    private void showEndGameState(GameStateChangedEvent event) {
        Player winner = getPlayer(event.getPlayers(), event.getWinnerId());
        whoWinText.setText(getString(R.string.winner_text, winner.getName()));
        whoWinText.setVisibility(View.VISIBLE);
        newLapBtn.setVisibility(View.GONE);
        newGameBtn.setVisibility(View.VISIBLE);
    }

    private void updatePlayerActionButtons(GameStateChangedEvent event) {
        disableAllPlayerActionButtons();
        Player realPlayer = getPlayer(event.getPlayers(), REAL_PLAYER_ID);
        if (realPlayer != null) {
            for (PlayerAction playerAction : realPlayer.getSupportedActions()) {
                switch (playerAction) {
                    case CHECK:
                        setUserActionEnabled(checkBtn, true);
                        break;
                    case CALL:
                        setUserActionEnabled(callBtn, true);
                        break;
                    case RAISE:
                        setUserActionEnabled(raiseBtn, true);
                        break;
                    case FOLD:
                        setUserActionEnabled(foldBtn, true);
                        break;
                }
            }
        }
    }

    private void updatePlayerViews(GameStateChangedEvent event) {
        for (Player player : event.getPlayers()) {
            if (player.getId().equals(REAL_PLAYER_ID)) {
                realPlayerView.show(player, true);
            } else if (player.getId().equals(AI_PLAYER_ID)) {
                aiPlayerView.show(player, event.getGamePhase() == GamePhase.END_LAP || event.getGamePhase() == GamePhase.END_GAME);
            } else {
                throw new IllegalStateException();
            }
        }
    }

    private void findViews() {
        realPlayerView = (PlayerView) findViewById(R.id.real_player_view);
        aiPlayerView = (PlayerView) findViewById(R.id.ai_player_view);
        bankText = (TextView) findViewById(R.id.bank_text);
        whoWinText = (TextView) findViewById(R.id.who_win_text);
        tableCardsView = (CardListView) findViewById(R.id.table_cards_view);
        newLapBtn = findViewById(R.id.new_lap_btn);
        newGameBtn = findViewById(R.id.new_game_btn);
        raiseBtn = findViewById(R.id.raise_btn);
        callBtn = findViewById(R.id.call_btn);
        checkBtn = findViewById(R.id.check_btn);
        foldBtn = findViewById(R.id.fold_btn);
    }

    private void initViews() {
        makeBlue(newLapBtn);
        makeBlue(newGameBtn);
    }

    private void makeBlue(View button) {
        button.getBackground().setColorFilter(ContextCompat.getColor(this, R.color.blue), PorterDuff.Mode.MULTIPLY);
    }

    private Player getPlayer(ArrayList<Player> players, String winnerId) {
        for (Player player : players) {
            if (player.getId().equals(winnerId)) {
                return player;
            }
        }
        return null;
    }

    private void initListeners() {
        newGameBtn.setOnClickListener(v -> gameManager.start());
        newLapBtn.setOnClickListener(v -> gameManager.nextLap());
        callBtn.setOnClickListener(v -> gameManager.onCall(REAL_PLAYER_ID));
        raiseBtn.setOnClickListener(v -> gameManager.onRaise(REAL_PLAYER_ID));
        foldBtn.setOnClickListener(v -> gameManager.onFold(REAL_PLAYER_ID));
        checkBtn.setOnClickListener(v -> gameManager.onCheck(REAL_PLAYER_ID));
    }

    private void disableAllPlayerActionButtons() {
        setUserActionEnabled(callBtn, false);
        setUserActionEnabled(raiseBtn, false);
        setUserActionEnabled(checkBtn, false);
        setUserActionEnabled(foldBtn, false);
    }

    private void setUserActionEnabled(View button, boolean enabled) {
        button.setEnabled(enabled);
        int colorId = enabled ? R.color.blue : R.color.gray;
        button.getBackground().setColorFilter(ContextCompat.getColor(this, colorId), PorterDuff.Mode.MULTIPLY);
    }

    private void tryMakeAiPlayerAction(GameStateChangedEvent event) {
        handler.postDelayed(() -> {
            Player aiPlayer = getPlayer(event.getPlayers(), AI_PLAYER_ID);
            if (aiPlayer.isActive()) {
                if (aiPlayer.getSupportedActions().contains(PlayerAction.CHECK)) {
                    gameManager.onCheck(AI_PLAYER_ID);
                } else if (aiPlayer.getSupportedActions().contains(PlayerAction.CALL)) {
                    gameManager.onCall(AI_PLAYER_ID);
                } else {
                    Random rnd = new Random();
                    PlayerAction playerAction = aiPlayer.getSupportedActions().get(rnd.nextInt(aiPlayer.getSupportedActions().size()));
                    switch (playerAction) {
                        case CHECK:
                            gameManager.onCheck(AI_PLAYER_ID);
                            break;
                        case CALL:
                            gameManager.onCall(AI_PLAYER_ID);
                            break;
                        case RAISE:
                            gameManager.onRaise(AI_PLAYER_ID);
                            break;
                        case FOLD:
                            gameManager.onFold(AI_PLAYER_ID);
                            break;
                    }
                }
            }
        }, 500);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (changingGameStateSubscription != null && !changingGameStateSubscription.isUnsubscribed()) {
            changingGameStateSubscription.unsubscribe();
        }
    }
}
