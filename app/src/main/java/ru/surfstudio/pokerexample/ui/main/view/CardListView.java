package ru.surfstudio.pokerexample.ui.main.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.surfstudio.pokerexample.R;
import ru.surfstudio.pokerexample.entities.Card;

public class CardListView extends LinearLayout {
    private static final int MAX_NUM_CARDS = 5;
    private List<CardView> cardViews = new ArrayList<>();

    public CardListView(Context context) {
        super(context);
        init(context);
    }

    public CardListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CardListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public CardListView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        setOrientation(HORIZONTAL);
        inflate(context, R.layout.card_list_view_layout, this);

        cardViews.add((CardView) findViewById(R.id.card_list_view_card1));
        cardViews.add((CardView) findViewById(R.id.card_list_view_card2));
        cardViews.add((CardView) findViewById(R.id.card_list_view_card3));
        cardViews.add((CardView) findViewById(R.id.card_list_view_card4));
        cardViews.add((CardView) findViewById(R.id.card_list_view_card5));
    }

    public void show(List<Card> cards) {
        hideAllCards();
        for (int i = 0; i < cards.size(); i++) {
            CardView cardView = cardViews.get(i);
            cardView.setVisibility(VISIBLE);
            cardView.show(cards.get(i));
        }
    }

    private void hideAllCards() {
        for (CardView cardView : cardViews) {
            cardView.setVisibility(GONE);
        }
    }
}
