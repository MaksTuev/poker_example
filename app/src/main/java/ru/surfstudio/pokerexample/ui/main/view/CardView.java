package ru.surfstudio.pokerexample.ui.main.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.TextView;

import ru.surfstudio.pokerexample.R;
import ru.surfstudio.pokerexample.entities.Card;
import ru.surfstudio.pokerexample.ui.TextUtil;

public class CardView extends FrameLayout {
    private TextView cardText;

    public CardView(Context context) {
        super(context);
        init(context);
    }

    public CardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public CardView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        inflate(context, R.layout.card_view_layout, this);
        cardText = (TextView)findViewById(R.id.card_value_text);
    }

    public void show(@Nullable Card card){
        if(card == null){
            cardText.setText("");
        } else {
            cardText.setText(TextUtil.getCardText(card));
        }
    }


}
