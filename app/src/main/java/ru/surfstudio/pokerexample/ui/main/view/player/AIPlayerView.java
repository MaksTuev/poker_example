package ru.surfstudio.pokerexample.ui.main.view.player;

import android.content.Context;
import android.util.AttributeSet;

import ru.surfstudio.pokerexample.R;

public class AIPlayerView extends PlayerView {
    public AIPlayerView(Context context) {
        super(context);
    }

    public AIPlayerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AIPlayerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public AIPlayerView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public int getPlayerViewLayout() {
        return R.layout.ai_player_layout;
    }


}
