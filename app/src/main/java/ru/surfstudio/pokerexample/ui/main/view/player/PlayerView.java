package ru.surfstudio.pokerexample.ui.main.view.player;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import ru.surfstudio.pokerexample.R;
import ru.surfstudio.pokerexample.entities.Card;
import ru.surfstudio.pokerexample.entities.Player;
import ru.surfstudio.pokerexample.module.combination.CardList;
import ru.surfstudio.pokerexample.ui.main.view.CardListView;

public abstract class PlayerView extends LinearLayout {
    private TextView playerNameText;
    private CardListView playerCardsListView;
    private TextView playerBetText;

    public PlayerView(Context context) {
        super(context);
        init(context);
    }

    public PlayerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public PlayerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public PlayerView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        inflate(getContext(), getPlayerViewLayout(), this);
        playerNameText = (TextView) findViewById(R.id.player_name);
        playerCardsListView = (CardListView) findViewById(R.id.player_card_list);
        playerBetText = (TextView) findViewById(R.id.player_bet_text);
    }

    public void show(Player player, boolean showCards) {
        playerNameText.setText(String.format(Locale.getDefault(), "%s (%d)", player.getName(), player.getCash()));
        playerNameText.setTextColor(player.isActive()
                ? ContextCompat.getColor(getContext(), R.color.blue)
                : ContextCompat.getColor(getContext(), R.color.gray));
        playerBetText.setText(player.getBet() != 0
                ? getResources().getString(R.string.player_bet_text, player.getBet())
                : "");
        playerCardsListView.show(showCards
                ? player.getCards()
                : gethiddenCards(player.getCards().size()));
    }

    @LayoutRes
    public abstract int getPlayerViewLayout();


    private ArrayList<Card> gethiddenCards(int size) {
        ArrayList<Card> hiddenCards = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            hiddenCards.add(null);
        }
        return hiddenCards;
    }
}
