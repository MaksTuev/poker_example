package ru.surfstudio.pokerexample.ui.main.view.player;

import android.content.Context;
import android.util.AttributeSet;

import ru.surfstudio.pokerexample.R;

public class RealPlayerView extends PlayerView {
    public RealPlayerView(Context context) {
        super(context);
    }

    public RealPlayerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RealPlayerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public RealPlayerView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public int getPlayerViewLayout() {
        return R.layout.real_player_layout;
    }


}
