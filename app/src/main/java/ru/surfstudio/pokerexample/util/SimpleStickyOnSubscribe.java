package ru.surfstudio.pokerexample.util;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Subscriber;

/**
 * При подписке, подписчик получет последний обьект из потока
 * @param <T>
 */
public class SimpleStickyOnSubscribe<T> implements Observable.OnSubscribe<T> {

    private List<Subscriber<? super T>> subscribers = new ArrayList<>();
    private T lastEmittedObj;

    @Override
    public void call(Subscriber<? super T> subscriber) {
        this.subscribers.add(subscriber);
        subscriber.onStart();
        if(lastEmittedObj != null){
            subscriber.onNext(lastEmittedObj);
        }
    }

    public void emit(T obj){
        lastEmittedObj = obj;
        for(int i = 0; i<subscribers.size(); i++){
            Subscriber<? super T> subscriber = subscribers.get(i);
            if(subscriber.isUnsubscribed()){
                subscribers.remove(i);
                i--;
            } else {
                subscriber.onNext(obj);
            }
        }
    }
}
